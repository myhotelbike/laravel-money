<?php

namespace MyHotelBike\LaravelMoneyTests;


class TestCase extends \Orchestra\Testbench\TestCase
{

    protected function getPackageProviders($app)
    {
        return [
            \MyHotelBike\LaravelMoney\Providers\ServiceProvider::class,
        ];
    }
}
