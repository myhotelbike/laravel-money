<?php


namespace MyHotelBike\LaravelMoney\Facades;


use Illuminate\Support\Facades\Facade;
use Money\Currency;
use Money\Money;

/**
 * @method static Money|null create(int|null $amount, Currency|string|null $currency = NULL, ?Money $default = NULL)
 * @method static Money|null createFromDecimal(string|null $amount, Currency|string|null $currency = NULL)
 * @method static Money|null createFromJson(string|null $value)
 * @method static string|null toDecimal(Money|null $money)
 * @method static int subunitFor(Currency $currency)
 * @method static array|null jsonSerialize(Money $money)
 * @method static bool equals(Money $a, Money $b)
 * @method static bool notEquals(Money $a, Money $b)
 * @method static Money min(Money $least, Money ...$monies)
 * @method static Money max(Money $most, Money ...$monies)
 *
 * @see \MyHotelBike\LaravelMoney\Contracts\MoneyFactory
 * @see \MyHotelBike\LaravelMoney\Support\DefaultMoneyFactory
 **/
class MoneyFactory extends Facade {

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() {
        return \MyHotelBike\LaravelMoney\Contracts\MoneyFactory::class;
    }

}
