<?php


namespace MyHotelBike\LaravelMoney\Support;


use Money\Currencies;
use Money\Currency;
use Money\Money;
use Money\MoneyFormatter;
use Money\MoneyParser;
use MyHotelBike\LaravelMoney\Contracts\MoneyFactory;

class DefaultMoneyFactory implements MoneyFactory {

    private Currencies $currencies;

    private MoneyParser $parser;

    private MoneyFormatter $formatter;

    private Currency $currency;

    public function __construct(Currencies $currencies, MoneyParser $parser, MoneyFormatter $formatter, Currency $currency) {
        $this->currencies = $currencies;
        $this->parser = $parser;
        $this->formatter = $formatter;
        $this->currency = $currency;
    }

    public function create(?int $amount, Currency|string|null $currency = NULL, ?Money $default = NULL) : ?Money {
        if ($amount === NULL) {
            return $default;
        }

        $currency = $this->getCurrency($currency);

        return new Money($amount, $currency);
    }

    public function createFromDecimal(?string $amount, Currency|string|null $currency = NULL) : ?Money {
        if ($amount === NULL) {
            return NULL;
        }

        $currency = $this->getCurrency($currency);

        return $this->parser->parse($amount, $currency);
    }

    public function createFromJson(object|array|string $value) : ?Money {
        if (is_object($value)) {
            return $this->create($value->amount, $value->currency);
        }

        if (is_array($value)) {
            return $this->create($value['amount'], $value['currency']);
        }

        return $this->createFromDecimal($value);
    }

    public function getCurrency(Currency|string|null $currency) : Currency {
        if ($currency instanceof Currency) {
            return $currency;
        }

        if (is_string($currency)) {
            return new Currency($currency);
        }

        return $this->currency;
    }

    public function toDecimal(?Money $money) : ?string {
        if ($money === NULL) {
            return NULL;
        }

        return $this->formatter->format($money);
    }

    public function subunitFor(Currency $currency) : int {
        return $this->currencies->subunitFor($currency);
    }

    public function jsonSerialize(?Money $money) : ?array {
        if ($money === NULL) {
            return NULL;
        }

        return [
            'amount'   => $money->getAmount(),
            'currency' => $money->getCurrency()->getCode(),
            'subunit'  => $this->subunitFor($money->getCurrency()),
        ];
    }

    public function equals(?Money $a, ?Money $b) : bool {
        if ($a === NULL && $b === NULL) {
            return TRUE;
        }

        if ($a === NULL || $b === NULL) {
            return FALSE;
        }

        return $a->equals($b);
    }

    public function notEquals(?Money $a, ?Money $b) : bool {
        return !$this->equals($a, $b);
    }

    public function min(?Money $least,?Money ...$monies) : ?Money {
        foreach ($monies as $money) {
            if ($least === NULL || ($money !== NULL && $money->lessThan($least))) {
                $least = $money;
            }
        }

        return $least;
    }

    public function max(?Money $most, ?Money ...$monies) : ?Money {
        foreach ($monies as $money) {
            if ($most === NULL || ($money !== NULL && $money->greaterThan($most))) {
                $most = $money;
            }
        }

        return $most;
    }
}
