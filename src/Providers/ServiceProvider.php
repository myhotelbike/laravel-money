<?php

namespace MyHotelBike\LaravelMoney\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use Money\Currency;
use Money\Formatter\DecimalMoneyFormatter;
use Money\Money;
use Money\MoneyFormatter;
use Money\MoneyParser;
use Money\Parser\DecimalMoneyParser;
use MyHotelBike\LaravelMoney\Support\DefaultMoneyFactory;
use MyHotelBike\LaravelSupport\Facades\HtmlFormatter;

class ServiceProvider extends BaseServiceProvider
{

    public array $bindings = [
        \MyHotelBike\LaravelMoney\Contracts\MoneyFactory::class => \MyHotelBike\LaravelMoney\Support\DefaultMoneyFactory::class,
    ];

    public array $singletons = [
        \Money\MoneyParser::class    => \Money\Parser\DecimalMoneyParser::class,
        \Money\Currencies::class     => \Money\Currencies\ISOCurrencies::class,
        \Money\MoneyFormatter::class => \Money\Formatter\IntlMoneyFormatter::class,
    ];

    public function register()
    {
    }

    public function boot()
    {
        $configPath = __DIR__.'/../../config/money.php';
        $this->mergeConfigFrom($configPath, 'money');
        $this->publishes([$configPath => config_path('money.php')], 'config');

        $this->app->singleton(Currency::class, function ($app) {
            return new Currency($app->config->get('money.currency'));
        });

        $this->app->bind(\NumberFormatter::class, function ($app) {
            $app_config = $app->config['app'];

            return new \NumberFormatter("{$app_config['language']}_{$app_config['country']}", \NumberFormatter::CURRENCY);
        });

        $this->app->when(DefaultMoneyFactory::class)
            ->needs(MoneyParser::class)
            ->give(function ($app) {
                return $app->make(DecimalMoneyParser::class);
            });

        $this->app->when(DefaultMoneyFactory::class)
            ->needs(MoneyFormatter::class)
            ->give(function ($app) {
                return $app->make(DecimalMoneyFormatter::class);
            });

        HtmlFormatter::setFormatter('money', function (?Money $money) {
            if ($money === null) {
                return '';
            }

            $str = app(MoneyFormatter::class)->format($money);

            return preg_replace('/[.,]0+$/', '', $str);
        });

        Blade::directive('money', function ($expression) {
            /* Use htmlentities() here in stead of e() to encode € for pdf generation. */
            return "<?php echo htmlentities(\MyHotelBike\LaravelSupport\Facades\HtmlFormatter::format('money', $expression)); ?>";
        });
    }

}
