<?php


namespace MyHotelBike\LaravelMoney\Models\Casts;


use Illuminate\Contracts\Database\Eloquent\CastsAttributes;

class Money implements CastsAttributes
{

    public function get($model, $key, $value, $attributes) : ?\Money\Money
    {
        if ($value === null) {
            return null;
        }

        return new \Money\Money($value, $model->currency);
    }

    public function set($model, $key, $value, $attributes) : ?string
    {
        if ($value === null) {
            return null;
        }

        if (!($value instanceof \Money\Money)) {
            throw new \Exception("Value should be an instance of \Money\Money.");
        }

        return $value->getAmount();
    }
}
