<?php


namespace MyHotelBike\LaravelMoney\Models\Casts;


use Illuminate\Contracts\Database\Eloquent\CastsAttributes;

class Currency implements CastsAttributes
{

    public function get($model, $key, $value, $attributes) : ?\Money\Currency
    {
        if ($value === null) {
            return null;
        }

        return new \Money\Currency($value);
    }

    public function set($model, $key, $value, $attributes) : ?string
    {
        if ($value === null) {
            return null;
        }

        if (!($value instanceof \Money\Currency)) {
            $value = new \Money\Currency($value);
        }

        return $value->getCode();
    }
}
