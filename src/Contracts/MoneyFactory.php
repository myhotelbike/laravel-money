<?php


namespace MyHotelBike\LaravelMoney\Contracts;


use Money\Currency;
use Money\Money;

interface MoneyFactory
{

    function create(?int $amount, Currency|string|null $currency = null, ?Money $default = null) : ?Money;

    function createFromDecimal(?string $amount, Currency|string|null $currency = null) : ?Money;

    function createFromJson(object|array|string $value) : ?Money;

    function toDecimal(?Money $money) : ?string;

    function subunitFor(Currency $currency) : int;

    function jsonSerialize(Money $money) : ?array;

    function equals(?Money $a, ?Money $b) : bool;

    function notEquals(?Money $a, ?Money $b) : bool;

    function min(?Money $least, ?Money ...$monies) : ?Money;

    function max(?Money $most, ?Money ...$monies) : ?Money;
}
